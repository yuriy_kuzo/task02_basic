package com.epat;

import java.util.Scanner;
/**
 * Java program that builds an array
 * by entered interval and Fibonacci sequance,
 * First 2 numbers of Fibonacci sequance are last 2 numbers
 * entered by user in array with interval.
 *
 *
 * @author Yuriy Kuzo
 * @version 1.0
 */
public final class Start {
    /**
     * private constructor.
     */
    private Start() {
    }
    /**
     * program entry point.
     * @param args - main method specification
     * private consructor
     */
    public static void main(final String[] args) {

        int fibSize;

        EvenOdd evenOddsArray = new EvenOdd();
        evenOddsArray.run();

        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nEnter size of Fibonacci array:");
        fibSize = scanner.nextInt();
        Fibonacci fibonacci = new Fibonacci(fibSize, evenOddsArray.getMaxOdd(),
                evenOddsArray.getMaxEven());
        fibonacci.run();
    }

}
