/**
 * this is main package.
 */
package com.epat;

import java.util.Scanner;

/**
 * Class describes Array in interval.
 */
public class EvenOdd {
    /**
     * first number of array.
     */
    private int firstNumb;

    /**
     * last number of array.
     */
    private int lastNumb;

    /**
     * summary of even numbers.
     */
    private int sumEven;

    /**
     * summary of odd numbers.
     */
    private  int sumOdd;

    /**
     * max even number.
     */
    private int maxEven;

    /**
     * max odd number.
     */
    private int maxOdd;

    /**
     * Constructor of EvenOdd class.
     */
    EvenOdd() {
        firstNumb = 0;
        lastNumb = 0;
        sumEven = 0;
        sumOdd = 0;
        maxEven = 0;
        maxOdd = 0;
    }

    /**
     * start work of class.
     */
    public final void run() {
        System.out.println("Enter interval: \n");
        Scanner scanner = new Scanner(System.in);
        firstNumb = scanner.nextInt();
        lastNumb = scanner.nextInt();
        printEven();
        printOdd();
        setMaxNumbers();
    }

    /**
     * setting max odd and even numbers.
     */
    public final void setMaxNumbers() {
        if (lastNumb % 2 == 0) {
            maxEven = lastNumb;
            maxOdd = lastNumb - 1;
        } else {
            maxOdd = lastNumb;
            maxEven = lastNumb - 1;
        }
    }

    /**
     * print odd numbers.
     */
    public final void printOdd() {
        System.out.println("\n\nOdd numbers from start: \n");
        int k;
        if (firstNumb % 2 == 0) {
            k = firstNumb + 1;
        } else {
            k = firstNumb;
        }

        for (int i = k; i <= lastNumb; i += 2) {
            System.out.print(i + " ");
            sumOdd += i;
        }
        System.out.println("\n\nSum of odd numbers: " + sumOdd);
    }
    /**
     * print even numbers.
     */
    public final void printEven() {
        System.out.println("Even numbers from end: \n");

        int m;
        if (lastNumb % 2 == 0) {
            m = lastNumb;
        } else {
            m = lastNumb - 1;
        }
        for (int i = m; i >= firstNumb; i -= 2) {
            System.out.print(i + " ");
            sumEven += i;
        }
        System.out.println("\n\nSum of even numbers: " + sumEven);
    }

    /**
     * @return max even element from fibonacci sequance.
     */
    public final int getMaxEven() {
        return maxEven;
    }

    /**
     * @return max odd element from fibonacci sequance.
     */
    public final int getMaxOdd() {
        return maxOdd;
    }

}
