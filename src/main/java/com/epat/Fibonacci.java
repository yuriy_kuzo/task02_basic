package com.epat;

/**
 * Class that builds Fibonacci sequance.
 */
public class Fibonacci {
    /**
     * array of fibonacci numbers.
     */
    private int[] fibArray;

    /**
     * size of the array.
     */
    private int fibSize;

    /**
     * number of odd numbers.
     */
    private int fibSumOdd; // number of odds

    /**
     * number of even numbers.
     */
    private int fibSumEven; // number of evens

    /**
     * percent of odd numbers.
     */
    private double fibPercentOdd;

    /**
     * percent of even numbers.
     */
    private double fibPercentEven;

    /**
     * maximum possible percent.
     */
    static final int MAX_PERCENT = 100;


    /**
     * Constructor of Fibonacci class.
     *
     * @param size  - size of sequance
     * @param numb1 - first number of sequance
     * @param numb2 - second number of sequance
     */
    Fibonacci(final int size, final int numb1, final int numb2) {
        fibSize = size;

        fibArray = new int[fibSize];
        fibArray[0] = numb1;
        fibArray[1] = numb2;
        buildFibArray();

        fibSumEven = 0;
        fibSumOdd = 0;
        fibPercentEven = 0;
        fibPercentOdd = 0;

    }

    /**
     * building a sequence.
     */
    public final void buildFibArray() {
        for (int i = 2; i < fibSize; i++) {
            fibArray[i] = fibArray[i - 2] + fibArray[i - 1];
        }
    }

    /**
     * start work of class.
     */
    public final void run() {
        calcOddEven();
        calcPercent();
        System.out.println("\nPercent of odd numbers:" + fibPercentOdd
                + "\nPercent of even numbers:" + fibPercentEven);
    }

    /**
     * calculating number of odd and even numbers.
     */
    public final void calcOddEven() {

        for (int i = 0; i < fibSize; i++) {
            if (fibArray[i] % 2 == 0) {
                fibSumOdd++;
            } else {
                fibSumEven++;
            }
        }
    }

    /**
     * calculating percent of odd and even numbers.
     */
    public final void calcPercent() {
        /** maximum percent.*/

        fibPercentOdd = (double) fibSumOdd / fibSize * MAX_PERCENT;
        fibPercentEven = (double) fibSumEven / fibSize * MAX_PERCENT;
    }


}
